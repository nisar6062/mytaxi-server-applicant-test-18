package com.mytaxi.domainvalue;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@JsonDeserialize(using = EngineTypeDeserializer.class)
public enum EngineType {
	DIESEL("DIESEL"), PETROL("PETROL"), ELECTRIC("ELECTRIC");
	private String text;

	EngineType(String text) {
		this.text = text;
	}

	public String getText() {
		return this.text;
	}

	public static EngineType fromText(String text) {
		for (EngineType r : EngineType.values()) {
			if (r.getText().equals(text)) {
				return r;
			}
		}
		throw new IllegalArgumentException();
	}
}
